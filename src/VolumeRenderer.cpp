#include "VolumeRenderer.h"

VolumeRenderer::VolumeRenderer()
{
}


VolumeRenderer::~VolumeRenderer()
{
}


void VolumeRenderer::render()
{
	// Print manual
	std::cout << std::endl << std::endl << "Welcome to VolumeRenderer!" << std::endl;
	std::cout << "Move the object with WASD or the arrow keys!" << std::endl;
	std::cout << "For zooming press + or -" << std::endl;
	std::cout << "Rotating around the x axis: X, y axis: Y, z axis: Z." << std::endl;
	std::cout << "Inverse rotating around those axes: x:B, y:N and z:M" << std::endl;
	std::cout << "Decrease the sampling rate with 1 and increase it with 2!" << std::endl;
	std::cout << "Change the render technique with 6 (X-Ray), 7 (First Hit), 8 (MIP), 9 (Alpha Compositing) and 0 (Gradient Shading)" << std::endl;
	std::cout << "Choose rendering settings for different models: H (Head), B (Beetle) and L (Lobster). This only has an effect on some render techniques." << std::endl;
	std::cout << "And don't forget to read the README.pdf!" << std::endl << std::endl << std::endl;



	shutDown = false;
	double screenFillingRatio = 0.95f;
	renderType = 0;
	initScreenParameters();

	//prepare volumes
	for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
		double vWidth = volumes[i]->width();
		double vHeight = volumes[i]->height();
		
		//scale
		double xMaxScale = screenWidth / vWidth;
		double yMaxScale = screenHeight / vHeight;
		double vScale = min(xMaxScale, yMaxScale);
		vScale *= screenFillingRatio;
		volumes[i]->setScale(vScale);
		std::cout << "Set Scale to: " << vScale << std::endl;

		//center
		glm::dvec3 startingPoint = glm::dvec3(int((screenWidth - vWidth*vScale) / 2), int((screenHeight - vHeight*vScale) / 2), 1000);
		volumes[i]->setStartPoint(startingPoint);
		// std::cout << "Set Starting point to: " << startingPoint.x << "," << startingPoint.y<<","<<startingPoint.z<< std::endl; DEBUG

	}

	// create image file
	Mat image(screenHeight, screenWidth, CV_8UC3, Scalar(0, 0, 0));
	namedWindow("Result Window", WINDOW_AUTOSIZE);
	// camera direction
	glm::dvec3 viewDirection = glm::dvec3(0, 0, 1);
	while (!shutDown){

		for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
			//std::cout << "Volume size: " << volumes[i]->size() << std::endl; // DEBUG

			// raycasting

			// all bounding planes represented by a point on it and normal vector
			std::vector<std::pair<glm::dvec3, glm::dvec3>> boundingPlanes = volumes[i]->getBoundingPlanes();

			volumes[i]->setRenderType(renderType);

			/*
			int pixelWithSomethingOnIt = 0;
			int pixelWithNothingOnIt = 0;
			int onlyOneIntersectionPoint = 0;
			int moreThanTwoIntersectionPoints = 0;
			*/

			//for every pixel
			for (int x = 0; x < screenWidth; x++){
				for (int y = 0; y < screenHeight; y++){
					
					// Calculate intersection points
					// Using notation and formula from:
					//https://en.wikipedia.org/wiki/Line%E2%80%93plane_intersection#Algebraic_form
					glm::dvec3 l = viewDirection; // viewing line direction
					glm::dvec3 l0 = glm::dvec3(x, y, 0); // viewing line starting point
					std::vector<glm::dvec3> intersectionPoints;
					for (std::pair<glm::dvec3, glm::dvec3> plane : boundingPlanes){
						glm::dvec3 p0 = plane.first;
						glm::dvec3 n = plane.second;
						double lDotN = glm::dot(l, n);
						if (lDotN != 0){ // vectors not parallel -> intersection point exists
							double d = glm::dot((p0 - l0), n) / lDotN;
							glm::dvec3 intersectionPoint = d*l + l0;
							//point on volume or offside?
							if (volumes[i]->isOnOrIn(intersectionPoint)){
								//save intersectionpoint
								intersectionPoints.push_back(intersectionPoint);
							}
						}
					}

					// 1 intersection point found
					if (intersectionPoints.size() == 1){ //sometimes at edges
						// onlyOneIntersectionPoint++;  DEBUG
						image.at<cv::Vec3b>(y, x)[0] = 0;
						image.at<cv::Vec3b>(y, x)[1] = 0;
						image.at<cv::Vec3b>(y, x)[2] = 0;
					}

					//enough intersection points found
					else if (intersectionPoints.size() >= 2){
						glm::dvec3 screenPoint = glm::dvec3(x, y, 0);
						glm::dvec3 entryPoint;
						glm::dvec3 exitPoint;
						
						// Find entry and exit point in intersection points
						entryPoint = intersectionPoints[0]; //init
						exitPoint = intersectionPoints[0];
						// Search for the nearest and the farthest intersection point
						//  This makes the algorithm stable. Works also when there are 3 or more interception points (corners)
						for (std::vector<glm::dvec3>::size_type p = 1; p != intersectionPoints.size(); p++) {
							if (entryPoint.z > intersectionPoints[p].z){
								entryPoint = intersectionPoints[p];
							}
							if (exitPoint.z < intersectionPoints[p].z){
								exitPoint = intersectionPoints[p];
							}
						}
						glm::dvec3 direction = glm::fastNormalize(exitPoint - screenPoint);

						// calculates the color or intensity value for the pixel
						glm::dvec3 color;
						if (renderType == 0){ //MIP
							color = volumes[i]->calcColor(entryPoint, direction);
						}
						else if (renderType == 1){ //Alpha Compositing
							color = volumes[i]->alphaCompositing(entryPoint, direction);
						}
						else if (renderType == 2){ // Gradient based shading
							color = volumes[i]->gradientShading(entryPoint, direction);
						}
						else if (renderType == 3){ // First Hit Rendering
							color = volumes[i]->firstHitRendering(entryPoint, direction);
						}
						else if (renderType == 4){ // FX-Ray
							color = volumes[i]->averageRendering(entryPoint, direction);
						}

						// set the pixel color in the image
						image.at<cv::Vec3b>(y, x)[0] = (int)round(color.r * 255);
						image.at<cv::Vec3b>(y, x)[1] = (int)round(color.g * 255);
						image.at<cv::Vec3b>(y, x)[2] = (int)round(color.b * 255);

						// pixelWithSomethingOnIt++; DEBUG
					}
					else { // No intersection points
						// pixelWithNothingOnIt++;  DEBUG
						// Set background to dark grey
						image.at<cv::Vec3b>(y, x)[0] = 10;
						image.at<cv::Vec3b>(y, x)[1] = 10;
						image.at<cv::Vec3b>(y, x)[2] = 10;
					}
				}
			}

			// Output debug information
			/* DEBUG
			std::cout << "Pixels with something to see: " << pixelWithSomethingOnIt << ". Pixels with nothing to see: " << pixelWithNothingOnIt << std::endl;
			if (onlyOneIntersectionPoint > 0){
				std::cerr << onlyOneIntersectionPoint << " pixels had only one intersection point (marked blue). There must always be 2! (or 0)" << std::endl;
			}
			if (moreThanTwoIntersectionPoints > 0){
				std::cerr << moreThanTwoIntersectionPoints << " pixels had more than two intersection points (marked green). There must always be 2! (or 0)" << std::endl;
			}
			*/


		}
		// show result
		imshow("Result Window", image);

		// Check for input
		handleInput();
	}
}

void VolumeRenderer::handleInput(){
	bool modified = false; // modified the volume(s) since the last rendering?
	// Check for keystrokes until something has been changed AND there are no more keystrokes. Then render.
	// First int: ms to wait  for first keystroke (short because maybe there are already some)
	// Second int: ms to wait for next keystroke (short because if there are no more and there already has been a keystroke we delay the next renderstep)
	for (int pressedButton = cv::waitKey(20); !(pressedButton == -1 && modified); pressedButton = cv::waitKey(100)){

		if (pressedButton != -1){ // no key pressed
			// std::cout << "Pressed key with ASCII code " << pressedButton << std::endl; DEBUG

			if (pressedButton == 27){ //ESC
				shutDown = true;
				std::cout << "Escaping..." << std::endl;
			}

			else{
				modified = true;

				// ZOOMING
				const double zoomFactor = 1.2f;
				const double translationStandardDiff = screenWidth/8;
				const double oneDegree = 2 * 3.14159 / 360; // 2*PI = 360�
				const double rotationAngle = 30 * oneDegree;

				if (pressedButton == 43){ // +
					std::cout << "Zoom +" << std::endl;
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						glm::dvec3 dimensions = volumes[i]->getScale()*glm::dvec3(volumes[i]->width(), volumes[i]->height(), volumes[i]->depth());
						volumes[i]->setScale(volumes[i]->getScale() * zoomFactor);
						volumes[i]->setStartPoint(volumes[i]->getStartPoint() - ((zoomFactor - 1.0f)*(0.5f)*dimensions));
					}
				}
				else if (pressedButton == 45){ // -
					std::cout << "Zoom -" << std::endl;
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						glm::dvec3 dimensions = volumes[i]->getScale()*glm::dvec3(volumes[i]->width(), volumes[i]->height(), volumes[i]->depth());
						volumes[i]->setScale(volumes[i]->getScale() * (1 / zoomFactor));
						volumes[i]->setStartPoint(volumes[i]->getStartPoint() + ((1.0f - 1.0f / zoomFactor)*(0.5f)*dimensions));
					}
				}

				//TRANSLATION
				else if (pressedButton == 2490368 || pressedButton == 119){ // up (up arrow or W)
					std::cout << "UP" << std::endl;
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						
						double diff = translationStandardDiff / volumes[i]->getScale();
						volumes[i]->setStartPoint(volumes[i]->getStartPoint() + glm::dvec3(0.0f, -diff, 0.0f));
					}
				}
				else if (pressedButton == 2555904 || pressedButton == 100){ // right (right arrow or D)
					std::cout << "RIGHT" << std::endl;
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						double diff = translationStandardDiff / volumes[i]->getScale();
						volumes[i]->setStartPoint(volumes[i]->getStartPoint() + glm::dvec3(diff, 0.0f, 0.0f));
					}
				}
				else if (pressedButton == 2621440 || pressedButton == 115){ // down (down arrow or S)
					std::cout << "DOWN" << std::endl;
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						double diff = translationStandardDiff / volumes[i]->getScale();
						volumes[i]->setStartPoint(volumes[i]->getStartPoint() + glm::dvec3(0.0f, diff, 0.0f));
					}
				}
				else if (pressedButton == 2424832 || pressedButton == 97){ // left (left arrow or A)
					std::cout << "LEFT" << std::endl;
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						double diff = translationStandardDiff / volumes[i]->getScale();
						volumes[i]->setStartPoint(volumes[i]->getStartPoint() + glm::dvec3(-diff, 0.0f, 0.0f));
					}
				}

				// ROTATION
				else if (pressedButton == 120){ // X
					std::cout << "X Rotation" << std::endl;
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						const double standardRotation = rotationAngle;
						volumes[i]->rotateX(standardRotation);
					}
				}
				else if (pressedButton == 121){ // Y
					std::cout << "Y Rotation" << std::endl;
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						const double standardRotation = rotationAngle;
						volumes[i]->rotateY(standardRotation);
					}
				}
				else if (pressedButton == 122){ // Z
					std::cout << "Z Rotation" << std::endl;
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						const double standardRotation = rotationAngle;
						volumes[i]->rotateZ(standardRotation);
					}
				}
				else if (pressedButton == 98){ // B (backward rotation X)
					std::cout << "Backward X Rotation" << std::endl;
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						const double standardRotation = -rotationAngle;
						volumes[i]->rotateX(standardRotation);
					}
				}
				else if (pressedButton == 110){ // N (backward rotation Y)
					std::cout << "Backward Y Rotation" << std::endl;
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						const double standardRotation = -rotationAngle;
						volumes[i]->rotateY(standardRotation);
					}
				}
				else if (pressedButton == 109){ // M (backward rotation Z)
					std::cout << "Backward Z Rotation" << std::endl;
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						const double standardRotation = -rotationAngle;
						volumes[i]->rotateZ(standardRotation);
					}
				}

				// Sampling Rate
				else if (pressedButton == 49){ // 1 (decrease sampling rate, increase sampling distance)
					std::cout << "Decrease Sampling Rate" << std::endl;
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						volumes[i]->setSamplingRateScale(volumes[i]->getSamplingRateScale() * 2);
					}
				}
				else if (pressedButton == 50){ // 2 (increase sampling rate, decrease sampling distance)
					std::cout << "Increase Sampling Rate" << std::endl;
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						volumes[i]->setSamplingRateScale(volumes[i]->getSamplingRateScale() / 2);
					}
				}

				// Render Type
				else if (pressedButton == 54){ // 6 (x-ray - average rendering)
					renderType = 4;
					std::cout << "Switched to X-Ray (Average Rendering)" << std::endl;
				}
				else if (pressedButton == 55){ // 7 (first hit)
					renderType = 3;
					std::cout << "Switched to First Hit Rendering" << std::endl;
				}
				else if (pressedButton == 56){ // 8 (MIP)
					renderType = 0;
					std::cout << "Switched to Maximum Intensity Projection" << std::endl;
				}
				else if (pressedButton == 57){ // 9 (Alpha Compositing)
					renderType = 1;
					std::cout << "Switched to Alpha Compositing" << std::endl;
				}
				else if (pressedButton == 48){ // 0 (Gradient Based Shading)
					renderType = 2;
					std::cout << "Switched to Gradient Based Shading" << std::endl;
				}

				// Transfer function
				else if (pressedButton == 104){ // h (head optimized)
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						volumes[i]->setTransferFunction(0);
					}
					std::cout << "Switched to head transfer function" << std::endl;
				}
				else if (pressedButton == 107){ // k (beetle optimized)
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						volumes[i]->setTransferFunction(1);
					}
					std::cout << "Switched to beetle transfer function" << std::endl;
				}
				else if (pressedButton == 108){ // l (lobster optimized)
					for (std::vector<Volume*>::size_type i = 0; i != volumes.size(); i++) {
						volumes[i]->setTransferFunction(2);
					}
					std::cout << "Switched to lobster transfer function" << std::endl;
				}
			}
		}
	}
}

// Add a volume to be rendered.
void VolumeRenderer::addVolume(Volume* v)
{
	volumes.push_back(v);
}

// Read settings from external file
void VolumeRenderer::initScreenParameters(){
	std::ifstream settingsFile("settings.txt");
	if (settingsFile.fail()){
		std::cout << "ERROR: Reading settingsfile failed!!! Using standard settings instead" << std::endl;
	}
	std::string type;
	int value;
	while (settingsFile >> type >> value){
		if (type == "width"){
			screenWidth = value;
		}
		else if (type == "height"){
			screenHeight = value;
		}
		else{
			std::cout << "WARNING: In settings.txt is a value that has not been processed because it's unknown: " << type << std::endl;
		}
	}
}
