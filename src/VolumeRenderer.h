#pragma once
#include <vector> 
#include "Volume.h"
#include <glm/gtx/fast_square_root.hpp>

#include <iostream>
#include <fstream>
#include <sstream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace cv;
using namespace std;

class VolumeRenderer
{
public:
	VolumeRenderer();
	~VolumeRenderer();
	void render();
	// Add a volume to be rendered.
	void addVolume(Volume* v);

private:
	std::vector<Volume*> volumes;
	void handleInput();

	bool shutDown;

	int screenWidth = 0;
	int screenHeight = 0;
	int renderType; // 0: MIP, 1: Alpha Compositing, 2: Gradient Shading, 3 First Hit Rendering, 4 xray/average rendering
	void initScreenParameters();
};

