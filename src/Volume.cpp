#include "Volume.h"

#include <math.h>


//-------------------------------------------------------------------------------------------------
// Voxel
//-------------------------------------------------------------------------------------------------

Voxel::Voxel()
{
	setValue(0.0f);
}

Voxel::Voxel(const Voxel &other)
{
	setValue(other.getValue());
}

Voxel::Voxel(const double value)
{
	setValue(value);
}

Voxel::~Voxel()
{
}

void Voxel::setValue(const double value)
{
	m_Value = value;
}

const double Voxel::getValue() const
{
	return m_Value;
};

const bool Voxel::operator==(const Voxel &other) const
{
	return (getValue() == other.getValue());
};

const bool Voxel::operator!=(const Voxel &other) const
{
	return !(*this == other);
};

const bool Voxel::operator>(const Voxel &other) const
{
	return getValue() > other.getValue();
};

const bool Voxel::operator>=(const Voxel &other) const
{
	return getValue() >= other.getValue();
};

const bool Voxel::operator<(const Voxel &other) const
{
	return getValue() < other.getValue();
};

const bool Voxel::operator<=(const Voxel &other) const
{
	return getValue() <= other.getValue();
};

const Voxel& Voxel::operator+=(const Voxel &other)
{
	m_Value += other.m_Value;
	return *this;
};

const Voxel& Voxel::operator-=(const Voxel &other)
{
	m_Value -= other.m_Value;
	return *this;
};

const Voxel& Voxel::operator*=(const double &value)
{
	m_Value *= value;
	return *this;
};

const Voxel& Voxel::operator/=(const double &value)
{
	m_Value /= value;
	return *this;
};

const Voxel Voxel::operator+(const Voxel &other) const
{
	Voxel voxNew = *this;
	voxNew += other;
	return voxNew;
};

const Voxel Voxel::operator-(const Voxel &other) const
{
	Voxel voxNew = *this;
	voxNew -= other;
	return voxNew;
};

const Voxel Voxel::operator*(const double &value) const
{
	Voxel voxNew = *this;
	voxNew *= value;
	return voxNew;
};

const Voxel Voxel::operator/(const double &value) const
{
	Voxel voxNew = *this;
	voxNew /= value;
	return voxNew;
};


//-------------------------------------------------------------------------------------------------
// Volume
//-------------------------------------------------------------------------------------------------

Volume::Volume()
	: m_Width(1), m_Height(1), m_Depth(1), m_Size(0), m_Voxels(1)
{
}

Volume::~Volume()
{
}

const Voxel& Volume::voxel(const int x, const int y, const int z) const
{
	return m_Voxels[x + y*m_Width + z*m_Width*m_Height];
}

const Voxel& Volume::voxel(const int i) const
{
	return m_Voxels[i];
}

const Voxel* Volume::voxels() const
{
	return &(m_Voxels.front());
};

const int Volume::width() const
{
	return m_Width;
};

const int Volume::height() const
{
	return m_Height;
};

const int Volume::depth() const
{
	return m_Depth;
};

const int Volume::size() const
{
	return m_Size;
};

void Volume::setStartPoint(glm::dvec3 startPoint){
	m_startingPoint = startPoint;
	calcCenterPoint();
}
glm::dvec3 Volume::getStartPoint(){
	return m_startingPoint;
}

void Volume::setScale(double scale){
	m_Scale = scale;
	calcCenterPoint();
}
double Volume::getScale(){
	return m_Scale;
}
void Volume::setSamplingRateScale(double scale){
	m_SamplingRateScale = scale;
}
double Volume::getSamplingRateScale(){
	return m_SamplingRateScale;
}

void Volume::calcCenterPoint(){
	m_centerPoint = m_startingPoint + glm::dvec3(m_Width,m_Height,m_Depth)*m_Scale*0.5;
}
void Volume::setTransferFunction(int i){
	transferFunction = i; 
	if (i == 0){ //head
		firstHitThreshold = 0.4f;
		lightDirection = glm::fastNormalize(glm::vec3(1, 1, -1));
		shininess = 15;
	}
	else if (i == 1){ //beetle
		firstHitThreshold = 0.05f;
		lightDirection = glm::fastNormalize(glm::vec3(1, 0, 15));
		shininess = 70;
	}
	else if (i == 2 ){//lobster
		firstHitThreshold = 0.2f;
		lightDirection = glm::fastNormalize(glm::vec3(1, 1, -1));
		shininess = 15;
	}
}
void Volume::setRenderType(int type){
	renderType = type;
}

//-------------------------------------------------------------------------------------------------
// Volume File Loader
//-------------------------------------------------------------------------------------------------
// startPoint: moves (one corner of the bounding box of) the volume to the given point
bool Volume::loadFromFile(QString filename, QProgressBar* progressBar, glm::dvec3 startPoint)
{
	// load file
	FILE *fp = NULL;
	fopen_s(&fp, filename.toStdString().c_str(), "rb");
	if (!fp)
	{
		std::cerr << "+ Error loading file: " << filename.toStdString() << std::endl;
		return false;
	}

	// progress bar

	progressBar->setRange(0, m_Size + 10);
	progressBar->setValue(0);


	// read header and set volume dimensions

	unsigned short uWidth, uHeight, uDepth;
	fread(&uWidth, sizeof(unsigned short), 1, fp);
	fread(&uHeight, sizeof(unsigned short), 1, fp);
	fread(&uDepth, sizeof(unsigned short), 1, fp);
	
	m_Width = int(uWidth);
	m_Height = int(uHeight);
	m_Depth = int(uDepth);
	m_startingPoint = startPoint;
	m_SamplingRateScale = 1.0;
	rot_mat = glm::dmat3();
	rot_mat_inv = glm::dmat3();
	lightDirection = glm::fastNormalize(glm::vec3(1,1, -1));
	shininess = 15;
	kDiffus = 0.7f;
	kSpecular = 0.8f;
	lightIntensity = 1.0f;
	kAmbient = 0.05f;

	// check dataset dimensions
	if (
			m_Width <= 0 || m_Width > 1000 ||
			m_Height <= 0 || m_Height > 1000 ||
			m_Depth <= 0 || m_Depth > 1000)
	{
		std::cerr << "+ Error loading file: " << filename.toStdString() << std::endl;
		std::cerr << "Unvalid dimensions - probably loaded .dat flow file instead of .gri file?" << std::endl;
		return false;
	}

	// compute dimensions
	int slice = m_Width * m_Height;
	m_Size = slice * m_Depth;
	int test = INT_MAX;
	m_Voxels.resize(m_Size);


	// read volume data

	// read into vector before writing data into volume to speed up process
	std::vector<unsigned short> vecData;
	vecData.resize(m_Size);
	fread((void*)&(vecData.front()), sizeof(unsigned short), m_Size, fp);
	fclose(fp);

	progressBar->setValue(10);


	// store volume data

	for (int i = 0; i < m_Size; i++)
	{
		// data is converted to double values in an interval of [0.0 .. 1.0];
		// uses 4095.0f to normalize the data, because only 12bit are used for the
		// data values, and then 4095.0f is the maximum possible value
		const double value = std::fmax(0.0f, std::fmin(1.0f, double(vecData[i]) / 4095.0f));
		m_Voxels[i] = Voxel(value);
		
		progressBar->setValue(10 + i);
	}

	progressBar->setValue(0);

	std::cout << "Loaded VOLUME with dimensions " << m_Width << " x " << m_Height << " x " << m_Depth << std::endl;

	return true;
}

std::vector<std::pair<glm::dvec3, glm::dvec3>> Volume::getBoundingPlanes(){
	
	//TEST -------------------------------------
	/*
	glm::dvec3 test = glm::dvec3(234, 423, 1166);
	glm::dvec3 rotated = rotate(test);
	glm::dvec3 reversed = rotateInv(rotated);
	if (abs(glm::length(test - reversed))>0.5){
		std::cout << "ERROR. Rotation Test failed. Original: " << test.x << "," << test.y << "," << test.z << "; reversed:" << reversed.x << "," << reversed.y << "," << reversed.z << "." << std::endl;
	}
	//other way round:
	rotated = rotateInv(test);
	reversed = rotate(rotated);
	if (abs(glm::length(test - reversed))>0.5){
		std::cout << "ERROR. Inverse Rotation Test failed. Original: " << test.x << "," << test.y << "," << test.z << "; reversed:" << reversed.x << "," << reversed.y << "," << reversed.z << "." << std::endl;
	}
	*/
	//TEST -------------------------------------

	glm::dvec3 x_rot = glm::cross(rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint) - rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint + glm::dvec3(0, 0, 1)), rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint) - rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint + glm::dvec3(0, 1, 0)));
	glm::dvec3 y_rot = glm::cross(rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint) - rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint + glm::dvec3(1, 0, 0)), rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint) - rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint + glm::dvec3(0, 0, 1)));
	glm::dvec3 z_rot = glm::cross(rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint) - rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint + glm::dvec3(1, 0, 0)), rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint) - rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint + glm::dvec3(0,1,0)));
	std::vector<std::pair<glm::dvec3, glm::dvec3>> boundingPlanes;
	boundingPlanes.push_back(std::pair<glm::dvec3, glm::dvec3>(rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint), z_rot));
	boundingPlanes.push_back(std::pair<glm::dvec3, glm::dvec3>(rotate(glm::dvec3(0, 0, m_Depth)*m_Scale + m_startingPoint), z_rot));
	boundingPlanes.push_back(std::pair<glm::dvec3, glm::dvec3>(rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint), x_rot));
	boundingPlanes.push_back(std::pair<glm::dvec3, glm::dvec3>(rotate(glm::dvec3(m_Width, 0, 0)*m_Scale + m_startingPoint), x_rot));
	boundingPlanes.push_back(std::pair<glm::dvec3, glm::dvec3>(rotate(glm::dvec3(0, 0, 0)*m_Scale + m_startingPoint), y_rot));
	boundingPlanes.push_back(std::pair<glm::dvec3, glm::dvec3>(rotate(glm::dvec3(0, m_Height, 0)*m_Scale + m_startingPoint), y_rot));
	return boundingPlanes;
}

void Volume::rotateX(double degree){ //https://de.wikipedia.org/wiki/Drehmatrix#Drehmatrizen_des_Raumes_R.C2.B3
	updateRotationMatrix(glm::dmat3(1, 0, 0, 0, cos(degree), -sin(degree), 0, sin(degree), cos(degree)));
	updateInverseRotationMatrix(glm::dmat3(1, 0, 0, 0, cos(-degree), -sin(-degree), 0, sin(-degree), cos(-degree)));
}
void Volume::rotateY(double degree){ //https://de.wikipedia.org/wiki/Drehmatrix#Drehmatrizen_des_Raumes_R.C2.B3
	updateRotationMatrix(glm::dmat3(cos(degree), 0, sin(degree), 0, 1, 0, -sin(degree), 0, cos(degree)));
	updateInverseRotationMatrix(glm::dmat3(cos(-degree), 0, sin(-degree), 0, 1, 0, -sin(-degree), 0, cos(-degree)));
}
void Volume::rotateZ(double degree){ //https://de.wikipedia.org/wiki/Drehmatrix#Drehmatrizen_des_Raumes_R.C2.B3
	updateRotationMatrix(glm::dmat3(cos(degree), -sin(degree), 0, sin(degree), cos(degree), 0, 0, 0, 1));
	updateInverseRotationMatrix(glm::dmat3(cos(-degree), -sin(-degree), 0, sin(-degree), cos(-degree), 0, 0, 0, 1));
}

void Volume::updateRotationMatrix(glm::dmat3 multiplyWithThis){
	rot_mat = multiplyWithThis * rot_mat;
}

void Volume::updateInverseRotationMatrix(glm::dmat3 multiplyWithThis){
	rot_mat_inv = glm::inverse(rot_mat);
}

/* 
Local coordinate system to world system
*/
glm::dvec3 Volume::rotate(glm::dvec3 in){
	return  (in - m_centerPoint) * rot_mat + m_centerPoint; //if you change this, please change also the mat inv operation below
}

/*
World coordinate system to local system
*/
glm::dvec3 Volume::rotateInv(glm::dvec3 in){
	return (in - m_centerPoint)* rot_mat_inv + m_centerPoint;
}

/* 
true if point is in or on the volume and
false if the point is outside the volume 
*/
bool Volume::isOnOrIn(glm::dvec3 point){
	//convert point
	point = (rotateInv(point) - m_startingPoint)/(glm::dvec3(m_Width,m_Height,m_Depth)*m_Scale);
	const double eps = 0.00001;
	if (point.x < 0 - eps || point.x > 1 + eps){
		return false;
	}
	if (point.y < 0 - eps || point.y > 1 + eps){
		return false;
	}
	if (point.z < 0 - eps || point.z > 1 + eps){
		return false;
	}
	return true;
}

/*
MIP Rendering
*/
glm::dvec3 Volume::calcColor(glm::dvec3 entryPoint, glm::dvec3 direction){
	glm::dvec3 anotherPoint = (rotateInv(entryPoint + direction) - m_startingPoint) / m_Scale;
	glm::dvec3 position = (rotateInv(entryPoint) - m_startingPoint) / m_Scale; // set point to entry point in volume coordinates
	
	direction = anotherPoint - position;
	double intensity = 0.0f;

	// iterate through the volume and search for biggest value on ray (MIP)
	/* nearest neighbor interpolation
	while (position.x >= 0 && position.x < m_Width - 1 && position.y >= 0 && position.y < m_Height - 1 && position.z >= 0 && position.z < m_Depth - 1){
		if (intensity < voxel(round(position.x), round(position.y), round(position.z)).getValue()){
			intensity = voxel(round(position.x), round(position.y), round(position.z)).getValue();
		}
		position = position + direction;
	} 
	*/
	
	// trilinear interpolation
	const double eps = 0.001;
	while (position.x >= 0.0 - eps && position.x < m_Width + eps && position.y >= 0.0 - eps && position.y < m_Height + eps && position.z >= 0.0 - eps && position.z < m_Depth + eps){
		float tempIntensity = calcIntensity(position);
		if (intensity < tempIntensity){
			intensity = tempIntensity;
		}
		position = position + m_SamplingRateScale*direction;
	}
	
	return intensityToColor(intensity);
}

/*
Gradient based shading with alpha compositing
*/
glm::dvec3 Volume::gradientShading(glm::dvec3 entryPoint, glm::dvec3 direction){
	glm::dvec3 anotherPoint = (rotateInv(entryPoint + direction) - m_startingPoint) / m_Scale;
	glm::dvec3 position = (rotateInv(entryPoint) - m_startingPoint) / m_Scale; // set point to entry point in volume coordinates

	direction = anotherPoint - position;
	double intensity = 0.0f;
	float treshold = 0.9f;
	// tilinear interpolation
	const double eps = 0.001;
	const bool alphaComp = false;
	
	glm::vec4 color = glm::vec4(0, 0, 0, 0);
	
	while (position.x >= 0.0 - eps && position.x < m_Width + eps && position.y >= 0.0 - eps && position.y < m_Height + eps && position.z >= 0.0 - eps && position.z < m_Depth + eps){
	
		const float intensity = calcIntensity(position);
		if (!alphaComp && intensity < firstHitThreshold){ 
			position = position + m_SamplingRateScale*direction;
			continue;
		}
		const glm::vec3 gradientAndIntensity = calcGradient(position);
		glm::vec3 gradient = glm::vec3(gradientAndIntensity.x, gradientAndIntensity.y, gradientAndIntensity.z);
		if (glm::length(gradient) < 0.25f){ 
			position = position + m_SamplingRateScale*direction;
			continue;
		}
		const glm::vec4 newColorWithTransparency = intensityToColorWithTransparency(intensity);
		glm::vec3 baseColor = glm::vec3(newColorWithTransparency.x, newColorWithTransparency.y, newColorWithTransparency.z);
		glm::vec4 newColor = glm::vec4(blinnPhong(gradient, baseColor, direction), newColorWithTransparency.w);
		
		if (alphaComp){												// Alpha Compositing
			color = (1 - color.a)*newColor.a*newColor + color;
			if (color.a > treshold){
				break;
			}
		}
		else{														// First hit
			if (intensity >= firstHitThreshold){
				color = newColor;
				break;
			}
		}
		position = position + m_SamplingRateScale*direction;
	}
	glm::vec3 resultColor = glm::vec3(color.r, color.g, color.b);
	
	float maxColor = glm::max(resultColor.r, glm::max(resultColor.g, resultColor.b));
	if (maxColor > 1){
		resultColor = resultColor / maxColor;
	}
	return resultColor;
}

glm::dvec3 Volume::alphaCompositing(glm::dvec3 entryPoint, glm::dvec3 direction){
	glm::dvec3 anotherPoint = (rotateInv(entryPoint + direction) - m_startingPoint) / m_Scale;
	glm::dvec3 position = (rotateInv(entryPoint) - m_startingPoint) / m_Scale;					// set point to entry point in volume coordinates

	direction = anotherPoint - position;
	double intensity = 0.0f;
	float treshold = 0.9f;
	// tilinear interpolation
	const double eps = 0.001;
	glm::vec4 color = glm::vec4(0,0,0,0);
	while (position.x >= 0.0 - eps && position.x < m_Width + eps && position.y >= 0.0 - eps && position.y < m_Height + eps && position.z >= 0.0 - eps && position.z < m_Depth + eps){
		const float tempIntensity = calcIntensity(position);
		const glm::vec4 newColor = intensityToColorWithTransparency(tempIntensity);
		color = (1 - color.a)*newColor.a*newColor + color;
		if (color.a>treshold){
			break;
		}
		position = position + m_SamplingRateScale*direction;
	}
	return glm::dvec3(color.r, color.g, color.b);
}

/*
First Hit Rendering
*/
glm::dvec3 Volume::firstHitRendering(glm::dvec3 entryPoint, glm::dvec3 direction){
	glm::dvec3 anotherPoint = (rotateInv(entryPoint + direction) - m_startingPoint) / m_Scale;
	glm::dvec3 position = (rotateInv(entryPoint) - m_startingPoint) / m_Scale; // set point to entry point in volume coordinates

	direction = anotherPoint - position;

	const double eps = 0.001;
	while (position.x >= 0.0 - eps && position.x < m_Width + eps && position.y >= 0.0 - eps && position.y < m_Height + eps && position.z >= 0.0 - eps && position.z < m_Depth + eps){
		float intensity = calcIntensity(position);
		if (firstHitThreshold < intensity){
			return intensityToColor(intensity);
		}
		position = position + m_SamplingRateScale*direction;
	}

	return intensityToColor(0.0f);
}

/*
X-Ray (Average Rendering)
*/
glm::dvec3 Volume::averageRendering(glm::dvec3 entryPoint, glm::dvec3 direction){
	glm::dvec3 anotherPoint = (rotateInv(entryPoint + direction) - m_startingPoint) / m_Scale;
	glm::dvec3 position = (rotateInv(entryPoint) - m_startingPoint) / m_Scale; // set point to entry point in volume coordinates

	const bool mathematicallyExact = false; //exact or more contrast?

	direction = anotherPoint - position;
	double intensity = 0.0f;
	int voxelCounter = 0;

	const double eps = 0.001;
	while (position.x >= 0.0 - eps && position.x < m_Width + eps && position.y >= 0.0 - eps && position.y < m_Height + eps && position.z >= 0.0 - eps && position.z < m_Depth + eps){
		float calculatedIntensity = calcIntensity(position);
		if (mathematicallyExact || calculatedIntensity >= 0.01){
			intensity += calculatedIntensity;
			voxelCounter++;
		}
		position = position + m_SamplingRateScale*direction;
	}

	intensity = intensity / glm::max(voxelCounter,1);
	if (mathematicallyExact){
		intensity = 2 * intensity;
	}
	if (intensity >= 1.0f) intensity = 1.0f;

	return intensityToColor(intensity);
}

/*
Calculates a color out of the given intensity based on a transfer function
*/
glm::dvec3 Volume::intensityToColor(float intensity){
	return glm::dvec3(intensity, intensity, intensity);
}

/*
alpha compositing transfer function
*/
glm::vec4 Volume::intensityToColorWithTransparency(float intensity){
	//bgr
	//head optimized
	if (transferFunction == 0){
		if (renderType == 2) return glm::vec4(0.65, 0.87, 0.95, intensity); //Gradient shading
		if (intensity > 0.4) return glm::vec4(0, 0, intensity, intensity);
		if (intensity > 0.175) return glm::vec4(0, intensity/1.5, intensity/1.5, intensity/1.5);
	}
	
	//beetle optimized
	else if (transferFunction == 1){
		if (renderType == 2) return glm::vec4(0.08,0.08,0.08,intensity); //Gradient shading
		if (intensity > 0.35) return glm::vec4(0, intensity, 0, intensity);
		if (intensity > 0.01) return glm::vec4(0, intensity / 1.5, intensity / 1.5, intensity / 1.5);
	}

	//lobster optimized
	else if (transferFunction == 2){
		if (renderType == 2) return glm::vec4(0, 0, 0.9f, intensity); //Gradient shading
		if (intensity > 0.6) return glm::vec4(0, 0, intensity, intensity);
		if (intensity > 0.01) return glm::vec4(0, intensity / 3, intensity / 1.75, intensity / 1.75);
	}
	
	return glm::vec4(intensity/4, intensity/4, intensity/4, intensity/4);
}

//calculates intensity with trilinear interpolation
float Volume::calcIntensity(glm::dvec3 position){
	if (position.x >= m_Width - 1 || position.y >= m_Height - 1 || position.z >= m_Depth - 1 ) return 0.0f;

	float floorX = std::min((double)m_Width-2, std::max(0.0,floor(position.x))); // -2 because below there is a +1
	float floorY = std::min((double)m_Height-2, std::max(0.0, floor(position.y)));
	float floorZ = std::min((double)m_Depth-2, std::max(0.0, floor(position.z)));

	float x = position.x - floorX;
	float y = position.y - floorY;
	float z = position.z - floorZ;

	float voxel1 = voxel(floorX, floorY, floorZ).getValue() * (1 - x) * (1 - y) * (1 - z);
	float voxel2 = voxel(floorX+1, floorY, floorZ).getValue() * x * (1 - y) * (1 - z);
	float voxel3 = voxel(floorX, floorY+1, floorZ).getValue() * (1 - x) * y * (1 - z);
	float voxel4 = voxel(floorX+1, floorY+1, floorZ).getValue() * x * y * (1 - z);
	float voxel5 = voxel(floorX, floorY, floorZ+1).getValue() * (1 - x) * (1 - y) * z;
	float voxel6 = voxel(floorX+1, floorY, floorZ+1).getValue() * x * (1 - y) * z;
	float voxel7 = voxel(floorX, floorY+1, floorZ+1).getValue() * (1 - x) * y * z;
	float voxel8 = voxel(floorX+1, floorY+1, floorZ+1).getValue() * x * y * z;

	float intensity = voxel1 + voxel2 + voxel3 + voxel4 + voxel5 + voxel6 + voxel7 + voxel8;
	return intensity; 

}

/*
Returns gradient (fx,fy,fz) not normalized and as a fourth coordinate the intensity of that point
*/
glm::vec3 Volume::calcGradient(glm::dvec3 position){
	const double dist = 1.0;
	
	// https://de.wikipedia.org/wiki/Differenzenquotient#Zentraler_Differenzenquotient
	float fx = calcIntensity(position + glm::dvec3(dist, 0, 0)) - calcIntensity(position - glm::dvec3(dist, 0, 0)) / (2 * dist);
	float fy = calcIntensity(position + glm::dvec3(0, dist, 0)) - calcIntensity(position - glm::dvec3(0, dist, 0)) / (2 * dist);
	float fz = calcIntensity(position + glm::dvec3(0, 0, dist)) - calcIntensity(position - glm::dvec3(0, 0, dist)) / (2 * dist);
	
	// Normalize
	return glm::fastNormalize(glm::vec3(fx, fy, fz));
}

glm::vec3 Volume::blinnPhong(glm::vec3 gradient, glm::vec3 color, glm::vec3 viewDirection){
	// normalize View Direction and 
	viewDirection = glm::fastNormalize(viewDirection);
	// Take the brighter direction of the light vector (enables lighting from both sides)
	float ldotn = glm::max(glm::dot(lightDirection, gradient), 0.0f);
	glm::vec3 H = (viewDirection + lightDirection) / glm::length(viewDirection + lightDirection);
	
	float ndoth = glm::max(glm::dot(gradient, H), 0.0f);
	glm::vec3 diffus = (lightIntensity*kDiffus*ldotn)*color;
	glm::vec3 specular = glm::vec3(lightIntensity*kSpecular*glm::pow(ndoth, shininess));
	glm::vec3 ambient = kAmbient*color;
	return specular + diffus + ambient;
}