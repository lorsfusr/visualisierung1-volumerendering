#pragma once

#include <vector>
#include <string>
#include <iostream>

#include <QProgressBar>


//-------------------------------------------------------------------------------------------------
// Multivariate Dataset
//-------------------------------------------------------------------------------------------------

class MultiSet
{

	public:

		MultiSet();
		~MultiSet();


		// DATA DIMENSIONS

		struct Variate
		{
			std::string		name;
			double			min;
			double			max;
		};

		const Variate&						variate(const int i) const;
		const Variate*						variates() const;

		const int							dimensions() const;


		// DATA VALUES

		struct DataElement
		{
			std::string						name;
			std::vector<double>				values;
		};

		const DataElement&					element(const int i) const;
		const DataElement*					elements() const;

		const double							value(const int i, const int v) const;

		const int							size() const;


		// FILE LOADER

		bool								loadFromFile(QString filename, QProgressBar* progressBar);


	private:

		std::vector<DataElement>			m_DataElements;
		std::vector<Variate>				m_Variates;

		int									m_Dimensions;
		int									m_Size;

};
