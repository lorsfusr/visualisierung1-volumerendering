#pragma once

#include <vector>
#include <string>
#include <iostream>
#include "glm/glm.hpp"
#include <glm/gtx/fast_square_root.hpp>

#include <QProgressBar>


//-------------------------------------------------------------------------------------------------
// Voxel
//-------------------------------------------------------------------------------------------------

class Voxel
{
	public:

		Voxel();
		Voxel(const Voxel &other);
		Voxel(const double value);

		~Voxel();


		// VOXEL VALUE

		void					setValue(const double value);
		const double				getValue() const;


		// OPERATORS

		const bool				operator==(const Voxel &other) const;
		const bool				operator!=(const Voxel &other) const;
		const bool				operator>(const Voxel &other) const;
		const bool				operator>=(const Voxel &other) const;
		const bool				operator<(const Voxel &other) const;
		const bool				operator<=(const Voxel &other) const;

		const Voxel				operator+(const Voxel &other) const;
		const Voxel				operator-(const Voxel &other) const;
		const Voxel				operator*(const double &value) const;
		const Voxel				operator/(const double &value) const;
		
		const Voxel&			operator+=(const Voxel &other);
		const Voxel&			operator-=(const Voxel &other);
		const Voxel&			operator*=(const double &value);
		const Voxel&			operator/=(const double &value);


	private:

		double					m_Value;

};


//-------------------------------------------------------------------------------------------------
// Volume
//-------------------------------------------------------------------------------------------------

class Volume
{

	public:

		Volume();
		~Volume();


		// VOLUME DATA

		const Voxel&			voxel(const int i) const;
		const Voxel&			voxel(const int x, const int y, const int z) const;
		const Voxel*			voxels() const;

		const int				width() const;
		const int				height() const;
		const int				depth() const;

		const int				size() const;
		void					setStartPoint(glm::dvec3 startPoint);
		glm::dvec3				getStartPoint();
		void					setScale(double scale);
		double					getScale();
		void					setSamplingRateScale(double scale);
		double					getSamplingRateScale();

		bool					loadFromFile(QString filename, QProgressBar* progressBar, glm::dvec3 startPoint);

		std::vector<std::pair<glm::dvec3, glm::dvec3>> getBoundingPlanes();

		bool					isOnOrIn(glm::dvec3 point);
		glm::dvec3				calcColor(glm::dvec3 entryPoint, glm::dvec3 exitPoint);
		glm::dvec3				gradientShading(glm::dvec3 entryPoint, glm::dvec3 direction);
		glm::dvec3				alphaCompositing(glm::dvec3 entryPoint, glm::dvec3 direction);
		glm::dvec3				firstHitRendering(glm::dvec3 entryPoint, glm::dvec3 direction);
		glm::dvec3				averageRendering(glm::dvec3 entryPoint, glm::dvec3 direction);

		void					rotateX(double degree);
		void					rotateY(double degree);
		void					rotateZ(double degree);

		glm::dvec3				rotate(glm::dvec3 in);
		glm::dvec3				rotateInv(glm::dvec3 in);
		void					updateRotationMatrix(glm::dmat3 multiplyWithThis);
		void					updateInverseRotationMatrix(glm::dmat3 multiplyWithThis);

		void					setRenderType(int type);
		void					setTransferFunction(int i);

	private:

		std::vector<Voxel>		m_Voxels;

		int						m_Width;
		int						m_Height;
		int						m_Depth;

		glm::dvec3				m_startingPoint;
		glm::dvec3				m_centerPoint;

		int						m_Size;

		double					m_Scale;
		double					m_SamplingRateScale;

		int						transferFunction = 0; // 0 = head, 1 = beetle, 2 = lobster
		float					firstHitThreshold = 0.4f;
		int						renderType = 0;

		glm::dmat3				rot_mat;
		glm::dmat3				rot_mat_inv;

		float					calcIntensity(glm::dvec3 position);
		glm::vec3				calcGradient(glm::dvec3 position);
		void					calcCenterPoint();
		glm::dvec3				intensityToColor(float intensity);
		glm::vec4				intensityToColorWithTransparency(float intensity);
		glm::vec3				blinnPhong(glm::vec3 gradient, glm::vec3 color, glm::vec3 viewDirection);

		// Blinn Phong:
		glm::vec3				lightDirection;
		int						shininess;
		float					kDiffus;
		float					kSpecular;
		float					kAmbient;
		float					lightIntensity;
};
