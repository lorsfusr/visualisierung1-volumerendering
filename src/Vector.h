#pragma once


// 2D-VECTOR
class Vector2
{

	public:

		Vector2();
		Vector2(const double x, const double y);
		Vector2(const double values[]);

		~Vector2();


		// VECTOR VALUES

		void values(const double x, const double y);
		const double* values() const;
		const double value(const unsigned int i) const;

		void x(const double x);
		void y(const double y);

		const double x() const;
		const double y() const;


		// VECTOR MATH

		const double magnitude() const;

		void normalise();
		void invert();

		const double dot(const Vector2 &other) const;


		// OPERATORS

		const bool operator==(const Vector2 &other) const;
		const bool operator!=(const Vector2 &other) const;

		const Vector2 operator-() const;

		const Vector2 operator+(const Vector2 &other) const;
		const Vector2 operator-(const Vector2 &other) const;
		const Vector2 operator*(const Vector2 &other) const;
		const Vector2 operator/(const Vector2 &other) const;

		const Vector2& operator+=(const Vector2 &other);
		const Vector2& operator-=(const Vector2 &other);
		const Vector2& operator*=(const Vector2 &other);
		const Vector2& operator/=(const Vector2 &other);

		const Vector2 operator*(const double &other) const;
		const Vector2 operator/(const double &other) const;

		const Vector2& operator*=(const double &other);
		const Vector2& operator/=(const double &other);

		double& operator[](const unsigned int i);
		const double operator[](const unsigned int i) const;


	private:

		double m_Elements[2];

};


// 3D-VECTOR
class Vector3
{
	public:

		Vector3();
		Vector3(const double x, const double y, const double z);
		Vector3(const double values[]);

		~Vector3();


		// VECTOR VALUES

		void values(const double x, const double y, const double z);
		const double* values() const;
		const double value(const unsigned int i) const;

		void x(const double x);
		void y(const double y);
		void z(const double z);

		const double x() const;
		const double y() const;
		const double z() const;


		// VECTOR MATH

		const double magnitude() const;

		void normalise();
		void invert();

		const double dot(const Vector3 &other) const;
		const Vector3 cross(const Vector3 &other) const;


		// OPERATORS
		
		const bool operator==(const Vector3 &other) const;
		const bool operator!=(const Vector3 &other) const;

		const Vector3 operator-() const;

		const Vector3 operator+(const Vector3 &other) const;
		const Vector3 operator-(const Vector3 &other) const;
		const Vector3 operator*(const Vector3 &other) const;
		const Vector3 operator/(const Vector3 &other) const;
		
		const Vector3& operator+=(const Vector3 &other);
		const Vector3& operator-=(const Vector3 &other);
		const Vector3& operator*=(const Vector3 &other);
		const Vector3& operator/=(const Vector3 &other);
		
		const Vector3 operator*(const double &other) const;
		const Vector3 operator/(const double &other) const;

		const Vector3& operator*=(const double &other);
		const Vector3& operator/=(const double &other);
		
		double& operator[](const unsigned int i);
		const double operator[](const unsigned int i) const;
		

	private:

		double m_Elements[3];
};
